"""
根据客户端请求，flask执行相应的url下的视图函数
例如：http://localhost:9000/index -->执行index()
"""
from blog_app import app #执行了__init__.py并得到app对象

@app.route('/')
@app.route('/index')
def index():
    return 'hello flask!'