from flask import Flask

app=Flask(__name__)  #__name__="blog_app"  package name，flask根据这个 package name 去寻找其他文件，例如template


# __init__.py是定义 package 并且可以被导入，导入的时候执行这个脚本，获得向外暴露的数据

from blog_app import routes  # 避免循环引入，因 blog_app package 已创建，才可以从里面导入相应的模块